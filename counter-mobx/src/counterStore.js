import {observable} from 'mobx';

// Use mobx observable to create a store
const counterStore = observable({
    counters: { a: 0, b: 0}
});

// Old style mobx, direct manipulation of store values.
// New style uses actions that are build into the store.

export default counterStore;
// function increaseAction(key, increment) {
//     console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);
//
//     const oldValue = store.counters[key];
//     store.counters[key] = oldValue + (increment || 1);
//
//     console.log('  new value: ', store.counters[key]);
// }
//
// const counterActions = {
//     increaseAction : (key) => increaseAction(key, 1), // Använder en funktion deklarerad ovan
//     decreaseAction : (key) => increaseAction(key, -1), // Använder en funktion deklarerad ovan
// };
//
// export {counterActions, counterStore};
