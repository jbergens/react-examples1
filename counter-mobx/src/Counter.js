import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NICE, SUPER_NICE, GOOD } from './colors';
// Using the store directly
import counterStore from './counterStore';

class Counter extends Component {
    render() {
        const props = this.props;
        const key = props.counter || 'a';
        const myValue = counterStore.counters[key] || 0; // Use the model/store
        console.log(`Counter[${key}].render()`, props);

        return (
            <div>
                <h1 style={{ color: props.color || GOOD }}>
                    Counter ({props.name}:{key}): {myValue}
                </h1>
            </div>
        );
    }
}

Counter.propTypes = {
    name: PropTypes.string.isRequired,
    counter: PropTypes.string,
    color: PropTypes.string
};

export default Counter;
