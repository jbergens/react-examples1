import counterStore from './counterStore';

// Old style mobx, direct manipulation of store values.
// New style uses actions that are build into the store.

function increaseAction(key, increment) {
    console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);

    const oldValue = counterStore.counters[key];
    counterStore.counters[key] = oldValue + (increment || 1);

    console.log('  new value: ', counterStore.counters[key]);
}

const counterActions = {
    increaseAction : (key) => increaseAction(key, 1), // Använder en funktion deklarerad ovan
    decreaseAction : (key) => increaseAction(key, -1), // Använder en funktion deklarerad ovan
};

export default counterActions;
