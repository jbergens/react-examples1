import React, {Component} from 'react';
import counterActions from './counterActions';
import CounterToolbarView from './CounterToolbarView';
import PropTypes from 'prop-types';

const CounterToolbar = (props) => {
    const key = props.counter || 'a';

    return (
        <CounterToolbarView counter={key}
            increaseAction={() => counterActions.increaseAction(key)}
            decreaseAction={() => counterActions.decreaseAction(key)}/>
    );
}
CounterToolbar.propTypes = {
    counter: PropTypes.string.isRequired
};

export default CounterToolbar;
