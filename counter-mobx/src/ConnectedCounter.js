import {observer} from 'mobx-react';
import Counter from './Counter';

// Wrap the ConnectedCounterImpl and connect it to the observer implementation in mobx
const ConnectedCounter = observer(Counter);

export default ConnectedCounter;
