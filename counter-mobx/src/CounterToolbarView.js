import React, {Component} from 'react';
import {actionCreators} from './counterStore';
import PropTypes from 'prop-types';

class CounterToolbarView extends Component  {
    increase() {
        if (this.props.increaseAction) {
            this.props.increaseAction(this.props.counter);
        }
    }

    decrease() {
        if (this.props.decreaseAction) {
            this.props.decreaseAction(this.props.counter);
        }
    }

    render() {
        return (
            <p> [{this.props.counter}]&nbsp;
                <button onClick={this.increase.bind(this)}>Increase</button>
                <button onClick={this.decrease.bind(this)}>Decrease</button>
            </p>
        );
    }
}
CounterToolbarView.propTypes = {
    counter: PropTypes.string.isRequired,
    increaseAction : PropTypes.func.isRequired,
    decreaseAction : PropTypes.func.isRequired
}

export default CounterToolbarView;
