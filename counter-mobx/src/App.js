import React, { Component } from 'react';
import MainView from './MainView';

export class App extends Component {
    render() {
        return (
            <MainView />
        );
    }
}
