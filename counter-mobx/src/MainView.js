import React, { Component } from 'react';
import { NICE, SUPER_NICE, GOOD } from './colors';
import ConnectedCounter from './ConnectedCounter';
import PureCounter from './PureCounter';
import CounterToolbar from './CounterToolbar';

export default class MainView extends Component {
    render() {
        return (
            <div>
                <h1>Example with Mobx</h1>
                <p>Using mobx to create some counters Try to change the code and
                    see what happens.</p>
                <ConnectedCounter name="Alfa" />
                <div>
                    Counters does not have to be placed next to each other.
                    <em>(TODO: Add more counters)</em>
                </div>

                <p>Buttons can be anywhere now.</p>
                <CounterToolbar counter="a" />
            </div>
        );
    }

    // <ConnectedCounter name="Beta" counter='b' color={NICE} />
    // <PureCounter name="Gamma" counter="a" />
    //
    // <CounterToolbar counter="b" />
}
