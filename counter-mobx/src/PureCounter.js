import React, { Component } from 'react';
import {observer} from 'mobx-react';
import { NICE, SUPER_NICE, GOOD } from './colors';
// The "store" is more of a model
import counterStore from './counterStore';

const PureCounterImpl = (props) => {
    const key = props.counter || 'a';
    const myValue = counterStore.counters[key] || 0; // Use the model/store
    console.log(`PureCounter[${key}].render()`, props);

    return (
        <div>
            <h1 style={{ color: props.color }}>
                PureCounter ({props.name}:{key}): {myValue}
            </h1>
        </div>
    );
};

const PureCounter = observer(PureCounterImpl); // Connect the component to MobX

export default PureCounter;
