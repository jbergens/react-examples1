import React, {Component} from 'react';
import {actionCreators} from './counterStore';
import CounterToolbarView from './CounterToolbarView';

const CounterToolbar = (props) => {
    const key = props.counter || 'a';

    return (
        <CounterToolbarView counter={key}
            increaseAction={() => actionCreators.increaseAction(key)}
            decreaseAction={() => actionCreators.decreaseAction(key)}/>
    );
}
CounterToolbar.propTypes = {
    counter: React.PropTypes.string.isRequired
};

export default CounterToolbar;
