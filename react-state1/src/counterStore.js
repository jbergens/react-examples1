import {observable} from 'mobx';

// Use mobx observable to create a store
let store = observable({
    counters: { a: 0, b: 0}
});

function increaseAction(key, increment) {
    console.log(`increaseAction was called. key: ${key}, increment: ${increment}`);

    const oldValue = store.counters[key];
    store.counters[key] = oldValue + (increment || 1);

    console.log('  new value: ', store.counters[key]);
}

const actionCreators = {
    increaseAction : (key) => increaseAction(key, 1), // Använder en funktion deklarerad ovan
    decreaseAction : (key) => increaseAction(key, -1), // Använder en funktion deklarerad ovan
};

export {actionCreators, store};
