import React, { Component } from 'react';
import MainView from './MainView';
import SimpleMainView from './Simple/SimpleMainView';

export class App extends Component {
    constructor() {
        super();
        this.currentView = 'simple';
        this.state = {
            currentView: 'simple'
        }
    }

    render() {
        if (this.currentView == 'simple') {
            return <SimpleMainView />;
        }
        if (this.state.currentView == 'simple') {
            return <SimpleMainView />;
        }

        // return (
        //     <SimpleMainView />
        // )

        return (
            <MainView />
        );
    }
}
