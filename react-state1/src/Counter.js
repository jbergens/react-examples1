import React, { Component } from 'react';
import { NICE, SUPER_NICE, GOOD } from './colors';
// Using the store directly
import {store} from './counterStore';

class Counter extends Component {
    render() {
        const props = this.props;
        const key = props.counter || 'a';
        const myValue = store.counters[key] || 0; // Use the model/store
        console.log(`Counter[${key}].render()`, props);

        return (
            <div>
                <h1 style={{ color: props.color || GOOD }}>
                    Counter ({props.name}:{key}): {myValue}
                </h1>
            </div>
        );
    }
}

Counter.propTypes = {
    name: React.PropTypes.string.isRequired,
    counter: React.PropTypes.string,
    color: React.PropTypes.string
};

export default Counter;
