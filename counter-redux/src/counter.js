import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NICE, SUPER_NICE, GOOD } from './colors';

class Counter extends Component {
    render() {
        const props = this.props;
        const myValue = props.value || 0;
        const myColor = props.color || GOOD;
        console.log('Counter.render()', props);

        return (
            <div>
                <h1 style={{ color: myColor }}>
                    Counter ({props.name}): {myValue}
                </h1>
            </div>
        );
    }
}

Counter.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.number,
    color: PropTypes.string
};

const PureCounter = (props) => {
    const myValue = props.value || 0;
    const myColor = props.color || GOOD;

    return (
        <div>
            <h1 style={{ color: myColor }}>
                PureCounter ({props.name}): {myValue}
            </h1>
        </div>
    );
};


export default Counter;
export {PureCounter};
