import React, { Component } from 'react';
import { Provider } from 'react-redux'
import Counter, {PureCounter} from './Counter';
import MainView from './MainView';
import {ActionCreators, Actions, store} from './counterStore';

export class App extends Component {
    componentDidMount() {
        store.subscribe(() => this.onCounterUpdated(store.getState()));
    }

    onCounterUpdated(state) {
        console.log('onCounterUpdated', state);
    }

    render() {
        return (
            <Provider store={store}>
                <MainView />
            </Provider>
        );
    }
}
