import {connect} from 'react-redux';
import Counter, {PureCounter} from './Counter';

function mapStateToProps(state) {
    console.log('mapStateToProps', state);
    return { value: state.counter };
};

// Koppla Counter till Redux store som kommer från Provider i App.js
// Detta blir en sorts "container component"
const ConnectedCounter = connect(mapStateToProps)(Counter);

// Det fungerar med pure render component också om vi bara skickar properties
const ConnectedPureCounter = connect(mapStateToProps)(PureCounter);

export default ConnectedCounter;
export {ConnectedPureCounter};
