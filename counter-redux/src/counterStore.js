import { createStore, combineReducers } from 'redux';

const actions = {
    INCREASE : 'INCREASE',
    DECREASE : 'DECREASE'
};

const counterReducer = function (state, action) {
    if (!state) {
        state = { counter: 0 }
    }
    console.log('counterReducer was called with state', state, 'and action', action)

    const oldCounter = state.counter || 0;

    switch (action.type) {
        case actions.INCREASE:
            return {
                counter: oldCounter + 1
            }
        case actions.DECREASE:
            return {
                counter: oldCounter - 1
            }
        default:
            return state;
    }
}

const store = createStore(counterReducer, {},
                          window.devToolsExtension && window.devToolsExtension());

const increaseActionCreator = function() {
    return { type: actions.INCREASE }
}

const actionCreators = {
    increaseAction : increaseActionCreator, // Använder en funktion delarerad ovan
    decreaseAction : () => { return { type: actions.DECREASE } } // Använder lambda
};

// Actions behövs inte utanför den här filen
export {actionCreators, store};
