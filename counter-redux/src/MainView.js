import React, { Component } from 'react';
import { NICE, SUPER_NICE, GOOD } from './colors';
import ConnectedCounter, {ConnectedPureCounter} from './ConnectedCounter';
import CounterToolbar from './CounterToolbar';
import {ActionCreators, Actions, store} from './counterStore';

export default class MainView extends Component {
    render() {
        return (
            <div>
                <h1>Example with Redux</h1>
                <p>Using Redux to hold some counters.</p>
                <ConnectedCounter name="Alfa" />
                <div>
                    Counters does not have to be placed next to each other.
                    <ConnectedCounter name="Beta" />
                    <ConnectedPureCounter name="Gamma" />
                </div>

                <p>Buttons can be anywhere now.</p>
                <CounterToolbar />
            </div>
        );
    }
}
