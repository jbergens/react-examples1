import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CounterToolbarView extends Component  {
    increase() {
        if (this.props.increaseAction) {
            this.props.increaseAction();
        }
    }

    decrease() {
        if (this.props.decreaseAction) {
            this.props.decreaseAction();
        }
    }

    render() {
        console.log("CounterToolbarView props:", this.props);
        return (
            <p>
                <button onClick={this.increase.bind(this)}>Increase</button>
                <button onClick={this.decrease.bind(this)}>Decrease</button>
            </p>
        );
    }
}
CounterToolbarView.propTypes = {
    increaseAction : PropTypes.func.isRequired,
    decreaseAction : PropTypes.func.isRequired
}

export default CounterToolbarView;
