//import React, {Component} from 'react';
//import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {actionCreators, store} from './counterStore';
import CounterToolbarView from './CounterToolbarView';

const CounterToolbar = connect(null, actionCreators)(CounterToolbarView);

export default CounterToolbar;
