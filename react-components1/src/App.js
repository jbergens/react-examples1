import React, { Component } from 'react';
import SimpleMainView from './Simple/SimpleMainView';

export class App extends Component {
    constructor() {
        super();
        this.currentView = 'simple';
        this.state = {
            currentView: 'simple'
        }
    }

    render() {
        if (this.state.currentView == 'simple') {
            return <SimpleMainView />;
        }

        return (
            <SimpleMainView />
        )
    }
}
