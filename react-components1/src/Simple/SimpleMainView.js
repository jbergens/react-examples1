import React, { Component } from 'react';
import { NICE, SUPER_NICE, GOOD } from '../colors';
import StdComponent from './StdComponent';
import FuncComponentA from './FuncComponentA';
import FuncComponentB from './FuncComponentB';

export default class SimpleMainView extends Component {
    render() {
        return (
            <div>
                <h1>Exempel med React komponenter</h1>
                <p>Test att ändra den här texten och titta i webbläsaren.
                Lägg på de extra komponenter som inte används än.</p>

                <StdComponent name="Kalle Anka"/>
                <FuncComponentB name="Kajsa" />
            </div>
        );
    }

    // render() {
    //     const component1 = (<StdComponent name="Kalle Anka"/>);
    //     const component2 = (<FuncComponentA name="Kajsa Anka"/>);
    //     const component3 = (<FuncComponentB name="Långben"/>);
    //
    //     return (
    //         <div>
    //             {this.renderIntro()}
    //             {component1}
    //         </div>
    //     );
    // }

    renderIntro() {
        return [
            <h1>Exempel med React komponenter</h1>,
            <p>Test att ändra den här texten och titta i webbläsaren.
                Lägg på de extra komponenter som inte används än.</p>
        ];
    }
}
